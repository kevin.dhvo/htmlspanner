package net.nightwhistler.htmlspanner.handlers;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;

import net.nightwhistler.htmlspanner.SpanStack;
import net.nightwhistler.htmlspanner.TagNodeHandler;
import net.nightwhistler.htmlspanner.style.Style;

import org.htmlcleaner.TagNode;

public class StrikeThroughHandler extends StyledTextHandler {

//    @Override
//    public void handleTagNode(final TagNode node, final SpannableStringBuilder builder, final int start, final int end, final SpanStack spanStack) {
//        builder.setSpan(new StrikethroughSpan(), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//    }

    @Override
    public Style getStyle() {
        return new Style().setTextDecoration(Style.TextDecoration.LINE_THROUGH);
    }

}
