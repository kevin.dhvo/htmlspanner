package net.nightwhistler.htmlspanner.handlers;

import android.text.SpannableStringBuilder;
import android.text.style.UnderlineSpan;

import net.nightwhistler.htmlspanner.SpanStack;
import net.nightwhistler.htmlspanner.TagNodeHandler;
import net.nightwhistler.htmlspanner.style.Style;

import org.htmlcleaner.TagNode;

public class UnderlineHandler extends StyledTextHandler {

//    @Override
//    public void handleTagNode(TagNode node, SpannableStringBuilder builder, int start, int end, SpanStack spanStack) {
//        spanStack.pushSpan(new UnderlineSpan(), start, end);
//    }

    @Override
    public Style getStyle() {
        return new Style().setTextDecoration(Style.TextDecoration.UNDERLINE);
    }

}
