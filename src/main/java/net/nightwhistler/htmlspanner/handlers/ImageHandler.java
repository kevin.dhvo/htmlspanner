/*
 * Copyright (C) 2011 Alex Kuiper <http://www.nightwhistler.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nightwhistler.htmlspanner.handlers;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import net.nightwhistler.androidsvg.SVG;
import net.nightwhistler.androidsvg.SVGParseException;
import net.nightwhistler.htmlspanner.SpanStack;
import net.nightwhistler.htmlspanner.TagNodeHandler;

import org.htmlcleaner.TagNode;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Base64;

/**
 * Handles image tags.
 * 
 * The default implementation tries to load images through a URL.openStream(),
 * override loadBitmap() to implement your own loading.
 * 
 * @author Alex Kuiper
 * 
 */
public class ImageHandler extends TagNodeHandler {

	@Override
	public void handleTagNode(TagNode node, SpannableStringBuilder builder,
			int start, int end, SpanStack stack) throws SVGParseException {
		String src = node.getAttributeByName("src");

		builder.append("\uFFFC");

		if (src.contains("data:image/svg+xml;base64,")) {
			String encodedSvg = src.substring(src.indexOf(",") + 1);
			Bitmap decodedSvg = imageFromString(encodedSvg);

			if (decodedSvg != null) {
				Drawable drawable = new BitmapDrawable(decodedSvg);
				drawable.setBounds(0, 0, decodedSvg.getWidth()*3,
						decodedSvg.getHeight()*3);

				stack.pushSpan( new ImageSpan(drawable), start, builder.length() );
			}
		} else if (src.contains(";base64,")) {
			String encodedImage = src.substring(src.indexOf(",") + 1);
			byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
			Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

			if (decodedImage != null) {
				Drawable drawable = new BitmapDrawable(decodedImage);
				drawable.setBounds(0, 0, decodedImage.getWidth()*3,
						decodedImage.getHeight()*3);

				stack.pushSpan( new ImageSpan(drawable), start, builder.length() );
			}
		} else {
			Bitmap bitmap = loadBitmap(src);

			if (bitmap != null) {
				Drawable drawable = new BitmapDrawable(bitmap);
				drawable.setBounds(0, 0, bitmap.getWidth()*3,
						bitmap.getHeight()*3);

				stack.pushSpan( new ImageSpan(drawable), start, builder.length() );
			}
		}

	}

	/**
	 * Loads a Bitmap from the given url.
	 * 
	 * @param url the given url
	 * @return a Bitmap, or null if it could not be loaded.
	 */
	protected Bitmap loadBitmap(String url) {
		try {
			return BitmapFactory.decodeStream(new URL(url).openStream());
		} catch (IOException io) {
			return null;
		}
	}

	private static Bitmap imageFromString(String imageData) throws SVGParseException {
		String data = imageData.substring(imageData.indexOf(",") + 1);
		byte[] imageAsBytes = Base64.decode(data.getBytes(), Base64.DEFAULT);
		String  svgAsString = new String(imageAsBytes, StandardCharsets.UTF_8);

		SVG svg = SVG.getFromString(svgAsString);

		// Create a bitmap and canvas to draw onto
		float svgWidth = (svg.getDocumentWidth() != -1) ? svg.getDocumentWidth() : 500f;
		float svgHeight = (svg.getDocumentHeight() != -1) ? svg.getDocumentHeight() : 500f;

		Bitmap  newBM = Bitmap.createBitmap(
				(int) Math.ceil(svgWidth),
				(int) Math.ceil(svgHeight),
				Bitmap.Config.ARGB_8888);
		Canvas bmcanvas = new Canvas(newBM);

		// Clear background to white if you want
//		bmcanvas.drawRGB(255, 255, 255);

		// Render our document onto our canvas
		svg.renderToCanvas(bmcanvas);

		return newBM;
	}
}
